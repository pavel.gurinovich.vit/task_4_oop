class Element {
  constructor(className, html) {
    this.className = className;
    this.html = html;
  }

  render() {
    const element = document.querySelector(`${this.className}`);
    if (element) {
      element.innerHTML = this.html;
    }
  }
}

class Image extends Element{
  constructor(className, src, alt=''){
    super(className)
    this.src = src
    this.alt = alt
  }


  createImage(){
    const image = document.createElement('img')
    image.src = this.src
    image.alt = this.alt
    return image
  }

  render(){
    const parentBlock = document.querySelector(`${this.className}`)
    const image = this.createImage()
    parentBlock.append(image)
  }
}


class Button extends Element{
  constructor(className, buttonId, buttonClassName){
    super(className)
    this.buttonId = buttonId
    this.buttonClassName = buttonClassName
  }
  
  createButton(){
    const button = document.createElement('button')
    button.id = this.buttonId
    button.className = this.buttonClassName
    return button
  }
  
  render(){
    const parentBlock = document.querySelector(`${this.className}`)
    const button = this.createButton()
    parentBlock.append(button)
  }

}


class ColorButton extends Button{
  constructor(className, buttonId, buttonClassName){
    super(className,buttonId, buttonClassName)
  }

  changeColor(){
    let flag = false
    document.getElementById(this.buttonId).textContent = 'Сменить тему'
    document.getElementById(this.buttonId).addEventListener("click", () =>{
      if(!flag){
          document.body.style.background = "#fcfcfc"
          document.getElementById("flex").style.color = '#292828'
          flag=true
      }else{
          document.body.style.background = "#262525"
          document.getElementById("flex").style.color = '#bdbdbd'
          flag = false
      }
  })  
  }
}


class AddButton{

  createField(parent, className){
      const newP = document.createElement('p')
      newP.className = className
      newP.innerHTML = 'New text field'
      parent.appendChild(newP);
      return newP
  }

  createButton(parent, id){
    const newButton = document.createElement('button')
    newButton.id = id
    newButton.textContent = 'Редактировать'
    parent.appendChild(newButton);
    return newButton
  }

  newButtonAction(button, note){
    const newButton = document.getElementById(button.id)
    let flag = true
    newButton.addEventListener("click", ()=>{
        if(flag){
            button.textContent = "Сохранить"
            note.contentEditable = true
            note.focus()
            flag = false
        }else{
            button.textContent = "Редактировать"
            note.contentEditable = false
            flag = true
        }
    })
    return true
  }
  newFunc(parentId, buttonId, noteClass, thisButtonId){
    document.getElementById(thisButtonId).remove()
    const parent = document.querySelector(parentId)
    const note = this.createField(parent, noteClass)
    const button = this.createButton(parent, buttonId)
    this.newButtonAction(button, note)
  }

}


const info_text = `
<h2>Контактная информация</h2>
<p>email: pashagurinovcih@gmail.com</p>
<p>Linkedin: DA</p>
<p>SOUZ NE RUSHIMII</p>
<p>RESPUBLIC SVOBODNIX</p>
<p>SPLOTILA NA VEKI VELIKAYA RUS'</p>
<h2>Рекомендации с прошлых мест работы</h2>
`
const lazyButton1 = new AddButton()
const button1_text = `
<button class="addButton" id="button1" onclick="lazyButton1.newFunc('.parent', 'someButton', 'someField', 'button1')">
Добавить информацию</button>
`
const title_text = `
<h1>Паша Гуринович</h1>
<h2>Пока еще не Dev</h2>
`
const personal_text = `
<div class="data">
    <h2>Личные данные</h2>
    <ul>
        <li>Lorem ipsum dolor sit amet</li>
        <li>consectetur adipiscing elit</li>
        <li>ex ea commodo consequat. </li>
    </ul>
</div>
<div class="data">
    <h2>Основные умения</h2>
    <ul>
        <li>Lorem ipsum dolor sit amet</li>
        <li>consectetur adipiscing elit</li>
        <li>ex ea commodo consequat. </li>
    </ul>
</div>
`
const exp_text = `
<h2>Опыт работы</h2>
<div class="job">
    <div class="data">
        <h3>Первая профессия</h3>
        <p>info</p>
    </div>
    <div class="info">
        <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
            <li>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</li>
            <li>ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
        </ul>
    </div>
</div>
<div class="job">
    <div class="data">
        <h3>Вторая профессия</h3>
        <p>Info </p>
    </div>
    <div class="info">
        <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
            <li>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</li>
            <li>ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
        </ul>
    </div>
</div>
<h2>Образование</h2>
<div class="job">
    <div class="data">
        <h3>БНТУ</h3>
        <p>КТО МЫ КТО МЫ КТО МЫ? </p>
    </div>
    <div class="info">
        
    </div>
</div>
`


const image = new Image(".img-container", "images/dige.jpg")
const info = new Element(".info", info_text)
const button1 = new Element(".parent", button1_text)

const colorButton = new ColorButton('.colorButton', '#btn', '.btn')
const title = new Element(".title", title_text);
const personal = new Element(".personal", personal_text);
const exp = new Element(".experience", exp_text)



image.render()
info.render()
button1.render()

colorButton.render()
colorButton.changeColor()
title.render()
personal.render()
exp.render()


